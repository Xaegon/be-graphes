package org.insa.graphs.algorithm.shortestpath;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.PriorityQueue;
import java.util.List;

import org.insa.graphs.algorithm.AbstractSolution.Status;
import org.insa.graphs.model.Arc;
import org.insa.graphs.model.Graph;
import org.insa.graphs.model.Node;
import org.insa.graphs.model.Path;
import org.insa.graphs.algorithm.utils.BinaryHeap;

public class DijkstraAlgorithm extends ShortestPathAlgorithm {

    public DijkstraAlgorithm(ShortestPathData data) {
        super(data);
    }
    
    protected Label createLabel(ShortestPathData data, Arc pere, Label noeudPre) {
    	if (pere != null && noeudPre != null) {
    		return new Label(pere.getDestination(), noeudPre.getCost()+data.getCost(pere), pere);
    	}
    	else {
    		return new Label(data.getOrigin(), 0, null);
    	}
    }
    
    @Override
    protected ShortestPathSolution doRun() {
        final ShortestPathData data = getInputData();
        Graph graph = data.getGraph();
        
        final int nbNodes = graph.size();
        
        //Create heap and array for labels
        BinaryHeap<Label> tas = new BinaryHeap<Label>();
        Label labels[] = new Label[graph.getNodes().size()];
        Arrays.fill(labels, null);
        
        Label origine = createLabel(data, null, null);
        labels[data.getOrigin().getId()] = origine;
        tas.insert(origine);
        
        Label mini, success;
        
        while (!tas.isEmpty() && (labels[data.getDestination().getId()] == null || !labels[data.getDestination().getId()].isMarked())) {
        	//Root recovery
        	mini = tas.findMin();
        	
        	if (mini.getCurrent() == data.getOrigin()) {
        		notifyOriginProcessed(data.getOrigin());
        	}
        	
        	if (mini.getCurrent() == data.getDestination()) {
        		notifyDestinationReached(data.getDestination());
        	}
        	
        	//Removing root
        	tas.remove(mini);
        	mini.setMark();
        	notifyNodeMarked(mini.getCurrent());
        	
        	//Successors of minimum node
        	for (Arc arc : mini.getCurrent().getSuccessors()) {
        		if (!data.isAllowed(arc)) {
        			continue;
        		}
        		success = labels[arc.getDestination().getId()];
        		
        		//Creation of label if doesn't exist
        		if (success == null) {
        			success = this.createLabel(data, arc, mini);
        			labels[arc.getDestination().getId()] = success;
        			tas.insert(success);
        			
        			notifyNodeReached(success.getCurrent());
        		}
        		//Successor cost optimization
        		else {
        			if (success.getCost() > mini.getCost() + data.getCost(arc)) {
        				success.setCost(mini.getCost() + data.getCost(arc));
        				success.setFather(arc);
        				
        			}
        		}
        	}
        }
        
        ShortestPathSolution solution = null;
        
        //Destination has been reached
        if (labels[data.getDestination().getId()] != null && labels[data.getDestination().getId()].isMarked()) {
        	ArrayList<Arc> SolArcs = new ArrayList<>();
        	Arc arc = labels[data.getDestination().getId()].getFather();
        	while (arc != null) {
        		SolArcs.add(arc);
        		arc = labels[arc.getOrigin().getId()].getFather();	
        	}
        	
        	Collections.reverse(SolArcs);
        	solution = new ShortestPathSolution(data, Status.OPTIMAL, new Path(graph, SolArcs));
        }
        else {
        	solution = new ShortestPathSolution(data, Status.INFEASIBLE);
        }
    
        return solution;
    }

}

